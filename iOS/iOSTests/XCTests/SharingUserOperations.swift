//
//  SharingUserOperations.swift
//  Tests
//
//  Created by Christopher Prince on 6/17/16.
//  Copyright © 2016 Spastic Muffin, LLC. All rights reserved.
//

import XCTest

class SharingUserOperations: BaseClass {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    
    // File upload by sharing user
    func testThatUploadOfNewFileByUnauthorizedSharingUserFails() {
    }
    
    func testThatUploadOfExistingFileByUnauthorizedSharingUserFails() {
    }
    
    func testThatUploadOfNewFileByAuthorizedSharingUserWorks() {
    }
    
    func testThatUploadOfExistingFileByAuthorizedSharingUserWorks() {
    }
}
